const baseUrl = 'https://www.omdbapi.com/';
const apikey = '&apikey=f6e256e1';
const plot = '&plot=full';

const main = document.getElementById('main');
const affiche = document.getElementById('afficheFilm');
const recherche = document.getElementById('search');


form.addEventListener('submit', (e) => {
    e.preventDefault();
    const searchTerm = search.value;
    const searchUrl = baseUrl + "?s=" + searchTerm + apikey;
    

    if(searchTerm){
        getMovies(searchUrl); 
    }else{
        getMovies(baseUrl);
    }
})

function getMovies(url){
    fetch(url)
    .then(response => response.json()).then(data => {
        showMovies(data);
    });
}

async function synopsis(syno) {
    const response = await fetch(syno)
    .then(data => data.json())
    .then(data => { 
        builHtml(data);
        return data; 
    });
    return response;
}

function showMovies(movies) {
    main.innerHTML = '';

    movies.Search.forEach(movie => {
        const searchSynopsis = baseUrl + "?t=" + movie.Title + plot + apikey ;
        let movieFull = synopsis(searchSynopsis);
    });
}

function builHtml (movie){
    let movieElement = document.createElement('div');
        movieElement.classList.add('movie');
        movieElement.innerHTML = `
            <img src="${movie.Poster}" alt="${movie.Title}">

            <div class="movie-info">
                <h2 class="title">${movie.Title}</h2>
            </div>
            <div class="plot">
                ${movie.Plot}
            </div>
            <div id="google_translate_element">
                <button type="submit" class="btnTrad btn btn-secondary">traduire</button>
            </div>
            <div class="year">
                ${movie.Year} - ${movie.Type}
            </div>
        `
        main.appendChild(movieElement);
}

function googleTranslateElementInit(){
    new googleTranslate.translate.TranslateElement({pageLanguage: 'en'},
    'google_translate_element');
}
